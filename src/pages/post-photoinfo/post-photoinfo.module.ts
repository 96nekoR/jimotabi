import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PostPhotoinfoPage } from './post-photoinfo';

@NgModule({
  declarations: [
    PostPhotoinfoPage,
  ],
  imports: [
    IonicPageModule.forChild(PostPhotoinfoPage),
  ],
})
export class PostPhotoinfoPageModule {}
