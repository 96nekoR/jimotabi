import { UserManageProvider } from './../../providers/user-manage/user-manage';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

import { PhotoInfo } from './../../model/photoinfo.model';
import { Observable } from 'rxjs/Observable';
import { finalize } from 'rxjs/operators';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { AngularFireStorage } from 'angularfire2/storage';

import { Camera, CameraOptions } from '@ionic-native/camera';

declare let Y: any;

@IonicPage()
@Component({
  selector: 'page-post-photoinfo',
  templateUrl: 'post-photoinfo.html',
})
export class PostPhotoinfoPage {

  private photoinfosCollection: AngularFirestoreCollection<PhotoInfo>;
  userID;

  imageURL:any;
  // フォームに入力した文字列を格納
  item: PhotoInfo;
  //GeoCoderで取得した情報を格納
  features: any[];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private storage: AngularFireStorage,
    private firestore: AngularFirestore,
    private alertCtrl: AlertController,
    private camera: Camera,
    private userManage: UserManageProvider,
  ) {
    this.cropImage();
    this.photoinfosCollection = this.firestore.collection<PhotoInfo>('PhotoInfos');
    this.item = {} as PhotoInfo;
    this.item.userID = this.userManage.getUserID();
    this.item.userEmail = this.userManage.getEmail();
    this.item.userIcon = this.userManage.myUserInfo.iconImage;
    this.item.userName = this.userManage.myUserInfo.name;
  }

    
  tapPostPhoto(){
    if(this.item.place == null || this.item.comment == null || this.item.category == null || this.item.latitude == null || this.item.longitude == null){
      this.presentAlert('投稿できません','記入項目を確認してください。');
    }else{
      this.uploadHandler();
    }
  }

  //  Storageに写真ファイルを投稿
  uploadHandler(){
    const filePath = "photo-" + this.item.userID +"-"+ new Date().getTime() + '.jpg';
    const fileRef = this.storage.ref(filePath);
    const task = fileRef.putString(this.imageURL,'data_url');
    task.snapshotChanges().pipe(
      finalize(() => {
        const downloadURL:Observable<string> = fileRef.getDownloadURL();
        this.uploadStorageItem(downloadURL);
      }))
      .subscribe()
  }

  //  itemのurlにStorageのURLを代入
  uploadStorageItem(_downloadURL: Observable<string>) {
    _downloadURL.subscribe((x: string) => {
      this.item.url = x;

      const curDate = new Date().getTime();
      const id = curDate + "-" + this.item.userID;
      this.item.id = id;
      this.photoinfosCollection.doc(id).set(this.item);
      this.navCtrl.pop();
    })
  }

  // https://alligator.io/ionic/angularfire2-file-uploads-ionic/
  getImage(){
    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum:true
    }
    this.camera.getPicture(options).then((ImageData) => {
      this.imageURL = 'data:image/jpeg;base64,' + ImageData;
    },(err)=>{
      console.log(err);
    });
  }


  cropImage(){
    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
      saveToPhotoAlbum:false,
      allowEdit:true,
      targetWidth:300,
      targetHeight:300
    }

    this.camera.getPicture(options).then((ImageData) => {
      this.imageURL = 'data:image/jpeg;base64,' + ImageData;
    },(err)=>{
      console.log(err);
    });
  }

  // 投稿できませんアラート
  presentAlert(title:string,subtitle:string) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: subtitle,
      buttons: ['OK']
    });
    alert.present();
  }

  getGeoCode(){
    if(this.item.place.length > 0){
      var geocoder = new Y.GeoCoder();
      geocoder.execute({query: this.item.place}, (response)=>{
        if(response.features.length > 0){
          this.features = new Array(response.features.length);
          for(var i = 0; i < response.features.length; i++){
            this.features[i] = response.features[i];
          }
        }
      });
    }else{
      this.features.length = 0;
    }
  }

  getLatLng(feature: any){
    this.item.place = feature.name;
    this.item.latitude = feature.latlng.lat();
    this.item.longitude = feature.latlng.lng();
    this.features.length = 0;
  }
}
