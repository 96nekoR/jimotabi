import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PostPlanInfoPage } from './post-plan-info';

@NgModule({
  declarations: [
    PostPlanInfoPage,
  ],
  imports: [
    IonicPageModule.forChild(PostPlanInfoPage),
  ],
})
export class PostPlanInfoPageModule {}
