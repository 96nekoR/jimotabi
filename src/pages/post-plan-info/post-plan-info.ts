import { UserManageProvider } from './../../providers/user-manage/user-manage';
import { PhotoInfo } from './../../model/photoinfo.model';
import { PlanInfo } from './../../model/planinfo.model';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, reorderArray, AlertController } from 'ionic-angular';
import { AngularFirestore } from 'angularfire2/firestore';
import { TabsPage } from '../tabs/tabs';

@IonicPage()
@Component({
  selector: 'page-post-plan-info',
  templateUrl: 'post-plan-info.html',
})
export class PostPlanInfoPage {

  //ギャラリーを開いているかどうか
  isOpenGallery = false;
  planInfo: PlanInfo;
  searchCategory: any = "すべて";

  photoGallery: PhotoInfo[][] = new Array();
  photoInfoList: PhotoInfo[] = new Array();

  constructor(public navCtrl: NavController, public navParams: NavParams, public firebase: AngularFirestore, public alertCtrl: AlertController, public userManage: UserManageProvider) {
    this.planInfo = {} as PlanInfo;
  }

  ionViewDidLoad() {
  }

  //データを並べ替える
  reorderData(indexes: any) {
    this.photoInfoList = reorderArray(this.photoInfoList, indexes);
  }

  //データを削除する
  deleteData(i: number){
    this.photoInfoList.splice(i, 1);
  }


  getFavoritePhotoList(): Promise<PhotoInfo[]>{
    var userInfoCollection = this.firebase.collection('UserInfos');
    var photoInfoCollection = userInfoCollection.doc(this.userManage.getEmail()).collection('favoritesPhoto');
    return new Promise(resolve=>{
      photoInfoCollection.valueChanges().subscribe((data:any) =>{
        resolve(data);
      });
    });
  }

  async openGallery(){
    this.isOpenGallery = true;
    let favoritePhotoList = await this.getFavoritePhotoList();
    var row = 0;
    for (let i = 0; i < favoritePhotoList.length; i+= 3) {
      this.photoGallery[row] = new Array();
      this.photoGallery[row] = favoritePhotoList.slice(i, i + 3);
      row++;
    }
  }

  async filterPhotoInfoList(){
    let favoritePhotoList = await this.getFavoritePhotoList();
    favoritePhotoList = favoritePhotoList.filter(photo => photo.category == this.searchCategory)
    this.initPhotoList();
    var row = 0;
    for (let i = 0; i < favoritePhotoList.length; i+= 3) {
      this.photoGallery[row] = new Array();
      this.photoGallery[row] = favoritePhotoList.slice(i, i + 3);
      row++;
    }
  }

  //photoInfoListの配列の中身を初期化
  initPhotoList(){
    for(var i=0; i<this.photoGallery.length; i++){
      this.photoGallery[i].length = 0;
    }
    this.photoGallery.length = 0;
  }

  selectPhoto(photo){
    var isSelectedPhoto: boolean = false;
    for(var i=0; i<this.photoInfoList.length; i++){
      if(this.photoInfoList[i].id == photo.id){
        isSelectedPhoto = true;
        break;
      }
    }

    if(isSelectedPhoto){
      this.alertToast("既に追加されています");
    }else{
      this.photoInfoList.push(photo);
      this.isOpenGallery = false;
    }
  }

  uploadPlan(){
    var id = this.firebase.createId();
    this.planInfo.id = id;
    this.planInfo.userID = this.userManage.getUserID();
    this.planInfo.userEmail = this.userManage.getEmail();

    if(this.planInfo.name != "" && this.planInfo.comment != "" && this.photoInfoList.length >= 2){
      this.firebase.collection("PlanInfos").doc(id).set(this.planInfo);
      for(var i=0; i<this.photoInfoList.length; i++){
        var photoInfo = this.photoInfoList[i];
        this.firebase.collection("PlanInfos").doc(id).collection("list").doc(photoInfo.id).set(photoInfo);
      }
      this.alertToast("投稿しました");
      this.navCtrl.setRoot(TabsPage);
    }else{
      this.alertToast("プラン内容が不十分です");
    }
  }

  alertToast(message){
    this.alertCtrl.create({
      title: message,
      buttons:['OK']
    }).present();
  }
}
