import { PlanInfo } from './../../model/planinfo.model';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { PhotoInfo } from '../../model/photoinfo.model';
import { RoutePage } from '../route/route';

@IonicPage()
@Component({
  selector: 'page-search-plan',
  templateUrl: 'search-plan.html',
})
export class SearchPlanPage {
  planInfoList: PlanInfo[] = new Array();
  photoInfoList: PhotoInfo[][] = new Array();

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController) {
    this.planInfoList = this.navParams.get('planInfoList');
    this.photoInfoList = this.navParams.get('photoInfoList');
  }

  ionViewDidLoad() {
  }

  showPrompt(plan: any){
    const prompt = this.alertCtrl.create({
      title: plan.name + 'を削除しますか？',
      buttons: [
        {
          text: '削除する',
          handler: data =>{
            alert("Delete clicked");
          }
        },
        {
          text: '削除しない',
          handler: data =>{
            alert("Cancel clicked");
          }
        }
      ]
    });
    prompt.present();
  }

  toPlanDetaiPage(planInfo: PlanInfo, i: number){
    this.navCtrl.push(RoutePage, {
      planInfo: planInfo,
      photoInfoList: this.photoInfoList[i]
    });
  }
}
