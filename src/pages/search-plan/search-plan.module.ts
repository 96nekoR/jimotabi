import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearchPlanPage } from './search-plan';

@NgModule({
  declarations: [
    SearchPlanPage,
  ],
  imports: [
    IonicPageModule.forChild(SearchPlanPage),
  ],
})
export class SearchPlanPageModule {}
