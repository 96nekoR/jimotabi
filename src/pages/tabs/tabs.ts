import { UserManageProvider } from './../../providers/user-manage/user-manage';
import { AngularFirestore } from 'angularfire2/firestore';
import { PostPhotoinfoPage } from '../post-photoinfo/post-photoinfo';
import { NavController, ActionSheetController, AlertController, ToastController } from 'ionic-angular';
import { Component } from '@angular/core';
import { HomePage } from '../home/home';
import { UserInfoPage } from '../user-info/user-info';
import { PostPlanInfoPage } from '../post-plan-info/post-plan-info';
import { RoutePage } from '../route/route';

@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = UserInfoPage;

  constructor(public navCtrl: NavController, private actionSheetCtrl: ActionSheetController, public firebase: AngularFirestore, public alertCtrl: AlertController, public userManage: UserManageProvider, public toastCtrl: ToastController) { }

  // 投稿ページへの遷移
  toPostPhotoInfoPage() {
    this.navCtrl.push(PostPhotoinfoPage);
  }

  toPostPlanInfo() {
    if (this.userManage.getStatus() == 'none') {
      this.showToast('ログインしてください');
      return;
    }else{
      this.navCtrl.push(PostPlanInfoPage);
    }
  }

  toPostTravelPlan() {
    if (this.userManage.getStatus() == 'none') {
      this.showToast('ログインしてください');
      return;
    } else {
      this.firebase.collection("UserInfos").doc(this.userManage.getEmail()).ref.get().then((doc: any) => {
        var travelId = doc.data().travelId;

        if (travelId != null) {
          this.firebase.collection('PlanInfos').doc(travelId).ref.get().then(doc => {
            var planInfo = doc.data();
            this.firebase.collection('PlanInfos').doc(travelId).collection('list').valueChanges().subscribe(data => {
              var photoInfoList = [];
              data.forEach((element) => {
                photoInfoList.push(element);
              });
              this.navCtrl.push(RoutePage, {
                planInfo: planInfo,
                photoInfoList: photoInfoList
              });
            });
          });
        } else {
          this.alertToast("旅行中のプランがありません");
        }
      });
    }
  }

  alertToast(message) {
    this.alertCtrl.create({
      title: message,
      buttons: ['OK']
    }).present();
  }

  switchPostPage() {
    const actionSheet = this.actionSheetCtrl.create({
      title: 'どっちを投稿する？',
      buttons: [
        {
          text: '写真',
          role: 'photo',
          handler: () => {
            this.toPostPhotoInfoPage();
          }
        }, {
          text: 'プラン',
          handler: () => {
            this.toPostPlanInfo();
          }
        }
      ]
    });
    actionSheet.present();
  }

  showToast(message){
    this.toastCtrl.create({
      message: message,
      duration: 3000,
    }).present();
  }
}
