import { UserManageProvider } from './../../providers/user-manage/user-manage';
import { PhotoInfo } from './../../model/photoinfo.model';
import { PlanInfo } from './../../model/planinfo.model';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, reorderArray, ToastController, AlertController } from 'ionic-angular';
import { AngularFirestore } from 'angularfire2/firestore';
import { TabsPage } from '../tabs/tabs';
import { EmailComposer } from '@ionic-native/email-composer';
declare let Y: any;

@IonicPage()
@Component({
  selector: 'page-route',
  templateUrl: 'route.html',
})
export class RoutePage {

  map: any;
  plugin: any;

  //旅行しているか
  isPlanTripping = false;
  //お気に入りにしているか
  isPlanSaved = false;
  //ギャラリーを開いているかどうか
  isOpenGallery = false;

  searchCategory: any = "すべて";

  planInfo: PlanInfo;
  photoInfoList: PhotoInfo[] = new Array();
  photoGallery: PhotoInfo[][] = new Array();

  constructor(public navCtrl: NavController, public navParams: NavParams, public toastCtrl: ToastController, public firebase: AngularFirestore, public alertCtrl: AlertController, public userManage: UserManageProvider,
    private emailComposer:EmailComposer) {
    this.planInfo = this.navParams.get('planInfo');
    this.photoInfoList = this.navParams.get('photoInfoList');
    this.isTrippingPlan();
    this.isFavoritePlan();
  }

  ionViewDidLoad() {
    this.showMap();
  }

  //地図を表示する
  //TODO: 地図に切り替えるとルートが表示されない問題を解決する。切り替えない場合は表示される
  showMap() {
    this.map = new Y.Map("my-map", {
      configure: {
        doubleClickZoom: true,
        scrollWheelZoom: true,
        dragging: true
      }
    });
    this.map.drawMap(new Y.LatLng(34.7015, 135.4965), 8, Y.LayerSetId.NORMAL);

    this.addRouteSearchPlugin();
  }

  //photoListに登録されている順序でルート検索をする
  addRouteSearchPlugin() {
    var configs = {
      "latlngs": []
    };

    for (var i = 0; i < this.photoInfoList.length; i++) {
      var photo = this.photoInfoList[i];
      configs.latlngs.push(new Y.LatLng(photo.latitude, photo.longitude));
    }
    this.plugin = new Y.RouteSearchPlugin(configs);

    this.map.addPlugin(this.plugin);
  }

  isTrippingPlan() {
    var docRef = this.firebase.collection('UserInfos').doc(this.userManage.getEmail()).ref;
    docRef.get().then((doc: any) => {
      if (doc.data().travelId == this.planInfo.id) {
        this.isPlanTripping = true;
      }
    });
  }

  //データを並べ替える
  reorderData(indexes: any) {
    this.photoInfoList = reorderArray(this.photoInfoList, indexes);
    this.map.removePlugin(this.plugin);
    this.addRouteSearchPlugin();
  }

  //データを削除する
  deleteData(i: number) {
    this.photoInfoList.splice(i, 1);
    this.map.removePlugin(this.plugin);
    this.addRouteSearchPlugin();
  }

  presentToast() {
    var message = "";
    if (this.userManage.getStatus() == 'none') {
      message = "ログインしてください";
    } else {
      this.isPlanSaved = !this.isPlanSaved;
      var userInfoCollection = this.firebase.collection('UserInfos');
      if (this.isPlanSaved) {
        message = "お気に入りに追加しました";
        userInfoCollection.doc(this.userManage.getEmail()).collection('favoritesPlan').doc(this.planInfo.id).set(this.planInfo);
        this.photoInfoList.forEach(photoInfo => {
          userInfoCollection.doc(this.userManage.getEmail()).collection('favoritesPlan').doc(this.planInfo.id).collection('list').doc(photoInfo.id).set(photoInfo);
        });
      } else {
        message = "お気に入りから削除しました";
        userInfoCollection.doc(this.userManage.getEmail()).collection('favoritesPlan').doc(this.planInfo.id).delete();
      }
    }
    const toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
    });
    toast.present();
  }

  isFavoritePlan() {
    if (this.userManage.getStatus() == 'none') {
      return;
    } else {
      var userInfoCollection = this.firebase.collection('UserInfos');
      var planList = userInfoCollection.doc(this.userManage.getEmail()).collection('favoritesPlan');
      planList.valueChanges().subscribe(data => {
        this.isPlanSaved = false;
        data.forEach((doc: any) => {
          if (doc.id == this.planInfo.id) {
            this.isPlanSaved = true;
          }
        });
      });
    }
  }

  selectTravelPlan() {
    if (this.userManage.getStatus() == 'none') {
      this.showToast('ログインしてください');
      return;
    } else {
      this.isPlanTripping = !this.isPlanTripping;
      if (this.isPlanTripping) {
        this.firebase.collection('UserInfos').doc(this.userManage.getEmail()).update({ travelId: this.planInfo.id });
      } else {
        this.firebase.collection('UserInfos').doc(this.userManage.getEmail()).update({ travelId: null });
      }
    }
  }

  getFavoritePhotoList(): Promise<PhotoInfo[]> {
    var userInfoCollection = this.firebase.collection('UserInfos');
    var photoInfoCollection = userInfoCollection.doc(this.userManage.getEmail()).collection('favoritesPhoto');
    return new Promise(resolve => {
      photoInfoCollection.valueChanges().subscribe((data: any) => {
        resolve(data);
      });
    });
  }

  async openGallery() {
    if (this.userManage.getStatus() == 'none') {
      this.showToast('ログインしてください');
      return;
    } else {
      this.isOpenGallery = true;
      let favoritePhotoList = await this.getFavoritePhotoList();
      var row = 0;
      for (let i = 0; i < favoritePhotoList.length; i += 3) {
        this.photoGallery[row] = new Array();
        this.photoGallery[row] = favoritePhotoList.slice(i, i + 3);
        row++;
      }
    }
  }

  async filterPhotoInfoList() {
    let favoritePhotoList = await this.getFavoritePhotoList();
    favoritePhotoList = favoritePhotoList.filter(photo => photo.category == this.searchCategory)
    this.initPhotoList();
    var row = 0;
    for (let i = 0; i < favoritePhotoList.length; i += 3) {
      this.photoGallery[row] = new Array();
      this.photoGallery[row] = favoritePhotoList.slice(i, i + 3);
      row++;
    }
  }

  //photoInfoListの配列の中身を初期化
  initPhotoList() {
    for (var i = 0; i < this.photoGallery.length; i++) {
      this.photoGallery[i].length = 0;
    }
    this.photoGallery.length = 0;
  }

  selectPhoto(photo) {
    var isSelectedPhoto: boolean = false;
    for (var i = 0; i < this.photoInfoList.length; i++) {
      if (this.photoInfoList[i].id == photo.id) {
        isSelectedPhoto = true;
        break;
      }
    }

    if (isSelectedPhoto) {
      this.alertToast("既に追加されています");
    } else {
      this.photoInfoList.push(photo);
      this.isOpenGallery = false;
      this.map.removePlugin(this.plugin);
      this.addRouteSearchPlugin();
    }
  }

  uploadPlan() {
    var id = this.firebase.createId();
    this.planInfo.id = id;
    this.planInfo.userID = this.userManage.getUserID();

    if (this.planInfo.name != "" && this.planInfo.comment != "" && this.photoInfoList.length >= 2) {
      this.firebase.collection("PlanInfos").doc(id).set(this.planInfo);
      for (var i = 0; i < this.photoInfoList.length; i++) {
        var photoInfo = this.photoInfoList[i];
        this.firebase.collection("PlanInfos").doc(id).collection("list").doc(photoInfo.id).set(photoInfo);
      }
      this.alertToast("投稿しました");
      this.navCtrl.setRoot(TabsPage);
    } else {
      this.alertToast("プラン内容が不十分です");
    }
  }

  alertToast(message) {
    this.alertCtrl.create({
      title: message,
      buttons: ['OK']
    }).present();
  }

  showToast(message){
    this.toastCtrl.create({
      message: message,
      duration: 3000,
    }).present();
  }

  sendEmail(_emailAdress, _type) {
    if (this.userManage.getStatus() == 'none') {
      this.presentToast();
      return;
    } else {
      let email = {
        to: _emailAdress,
        cc: '',
        bcc: [],
        attachments: [],
        subject: 'この' + _type + 'を案内してくれませんか？',
        body: this.userManage.myUserInfo.name + "と申します。" + 'ジモタビを利用して連絡しました。',
        isHtml: true
      };
      this.emailComposer.open(email);
    }
  }
}
