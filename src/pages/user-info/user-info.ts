import { UserEditPage } from './../user-edit/user-edit';
import { PlanInfo } from './../../model/planinfo.model';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';

import { UserInfo } from './../../model/userinfo.model';
import { PhotoInfo } from './../../model/photoinfo.model';

import { UserManageProvider } from './../../providers/user-manage/user-manage';

import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';

import { UserSignupPage } from './../user-signup/user-signup';
import { PhotoDetailPage } from './../photo-detail/photo-detail';
import { RoutePage } from '../route/route';


@IonicPage()
@Component({
  selector: 'page-user-info',
  templateUrl: 'user-info.html',
})
export class UserInfoPage {

  
  userSignUpPage: any;

  loginEmail:string;
  loginPass:string;
  EMAIL_KEY = 'key_email';
  PASS_KEY = 'pass_email';

  myUserInfo: UserInfo;

  myPhotoInfoList = [];
  myPlanInfoList = [];
  myPlanPhotoInfoList = [];

  //ページ切り替え
  LOGIN = 1;
  MYPAGE = 2;
  pageNum = this.LOGIN;
  
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private firestore: AngularFirestore,
    private afAuth: AngularFireAuth,
    private toast: ToastController,
    private userManage: UserManageProvider
  ) {
    this.autoLogin();
    this.userSignUpPage = UserSignupPage;
    this.myUserInfo = {} as UserInfo;
  }

  // ログイン
  async login(_email,_password) {
    console.log(_email + "|" + _password);

    this.afAuth
      .auth
      .signInAndRetrieveDataWithEmailAndPassword(_email, _password)
      .then(res => {
        if (res.user.email && res.user.uid) {
          console.log("ユーザー情報:"+ res.user.email + "|" + res.user.uid + "|" + _password)
          this.userManage.uid = res.user.uid;
          this.getMyUserInfo(res.user.email);
          this.setLocalStorageUserinfo(_email,_password);
          this.userManage.setStatus('login');
          this.userManage.setEmail(res.user.email);
          this.pageNum = this.MYPAGE;
        } else {
          this.presentToast(`Could not find authentication details.`);
        }
      })
      .catch(err => {
        this.presentToast(err.message);
      });
  }

  // 自動ログイン
  autoLogin(){    
    var jsonEmail:string = JSON.parse(localStorage.getItem(this.EMAIL_KEY));
    var jsonPass:string = JSON.parse(localStorage.getItem(this.PASS_KEY));
    console.log("value: "+jsonEmail);
    console.log("value: "+jsonPass);

    if(jsonEmail == null && jsonPass == null){
      console.log("未ログイン");
    }else{

      this.login(jsonEmail,jsonPass);
      console.log("ログイン済み");
    }
  }

  setLocalStorageUserinfo(_email,_password){
    localStorage.setItem(this.EMAIL_KEY,JSON.stringify(_email));
    localStorage.setItem(this.PASS_KEY,JSON.stringify(_password));
  }

  clearLocalStorageUserinfo(){
    localStorage.removeItem(this.EMAIL_KEY);
    localStorage.removeItem(this.PASS_KEY);
  }

  getMyUserInfo(_email) {
    var userinfosCollection = this.firestore.collection<UserInfo>('UserInfos');
    userinfosCollection.doc(_email).valueChanges()
      .subscribe((userinfo: any) => {
        this.myUserInfo = this.userManage.myUserInfo = userinfo;
        this.userManage.userID = this.userManage.myUserInfo.ID;
        console.log(this.userManage.userID);
        this.createPhotoInfoList(this.userManage.userID);
        this.createPlanInfoList(this.userManage.userID);
      });
  }


  //　サインアウト
  logOut() {
    this.afAuth
      .auth
      .signOut()
      .then(res => {
        this.clearLocalStorageUserinfo();
        this.presentToast("サインアウトしました");
        this.myUserInfo = null;
        this.userManage.myUserInfo = null;
        this.myPhotoInfoList = [];
        this.myPlanInfoList = [];
        this.myPlanPhotoInfoList = [];
        this.pageNum = this.LOGIN;
        this.userManage.setStatus('none');
      })
      .catch(function (err) {
        this.presentToast(err.message);
      });
  }


  // 自分の写真のリスト作成 
  createPhotoInfoList(_userID) {
    var photoInfoCollection: AngularFirestoreCollection<PhotoInfo> = this.firestore.collection('PhotoInfos');
    var allPhotoInfoList = [];
    console.log("createphotolist", this.myUserInfo.ID);
    let myPhotoIndex = 0;
    photoInfoCollection.valueChanges().subscribe(data => {
      allPhotoInfoList = data;
      for (let i = 0; i < allPhotoInfoList.length; i++) {
        if (allPhotoInfoList[i].userID == this.myUserInfo.ID && myPhotoIndex < 5) {
          this.myPhotoInfoList[myPhotoIndex] = allPhotoInfoList[i];
          myPhotoIndex++;
        }
      }
    })
  }
  
  // 自分のプランのリスト作成 
  createPlanInfoList(_userID) {
    var planInfoCollection: AngularFirestoreCollection<PlanInfo> = this.firestore.collection('PlanInfos');

    planInfoCollection.valueChanges().subscribe(allPlandata => {
      var i = 0;
      var myPlanIndex = 0;
      allPlandata.forEach((plan: any) => {
        if (plan.userID == this.myUserInfo.ID && myPlanIndex < 5) {
          var planPhotoInfoCollection = planInfoCollection.doc(plan.id).collection('list');
          planPhotoInfoCollection.valueChanges().subscribe(photodata => {
            this.myPlanPhotoInfoList[i] = new Array();
            photodata.forEach(photo => {
              this.myPlanPhotoInfoList[i].push(photo);
            });
            myPlanIndex++;
            i++;
          })
          this.myPlanInfoList.push(plan);
        }
      });
    })
  }

  // ユーザ情報編集ページへ遷移
  toUserEditPage(myUserInfo: UserInfo) {
    this.navCtrl.push(UserEditPage, {
      userInfo: myUserInfo
    });
  }
  
  // 写真詳細ページへ遷移
  toPhotoDetailPage(photo: PhotoInfo) {
    this.navCtrl.push(PhotoDetailPage, {
      item: photo
    });
  }
  
  // プラン詳細ページへ遷移
  toPlanDetaiPage(planInfo: PlanInfo, i: number) {
    this.navCtrl.push(RoutePage, {
      planInfo: planInfo,
      photoInfoList: this.myPlanPhotoInfoList[i]
    });
  }
  
  // トーストの表示
  presentToast(textMessage) {
    this.toast.create({
      message: textMessage,
      duration: 3000
    }).present();
  }

}
