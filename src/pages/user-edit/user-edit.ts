import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';

import { UserInfo } from './../../model/userinfo.model';

@IonicPage()
@Component({
  selector: 'page-user-edit',
  templateUrl: 'user-edit.html',
})
export class UserEditPage {
  
  userInfo:UserInfo;
  private userinfosCollection: AngularFirestoreCollection<UserInfo>;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private firestore: AngularFirestore,
    private toast:ToastController   
    ) {
    this.userInfo = navParams.get('userInfo');
  }

  editUserInfo(){
    this.userinfosCollection = this.firestore.collection<UserInfo>('UserInfos');
    const id = this.userInfo.email;
    this.userinfosCollection.doc(id).set(this.userInfo)
    .then(()=>{
      this.presentToast("編集できました。");
      this.navCtrl.pop();
    });
  }

  presentToast(textMessage) {
    this.toast.create({
      message: textMessage,
      duration: 3000
    }).present();
  }

}
