import { UserInfo } from './../../model/userinfo.model';
import { AngularFirestoreCollection, AngularFirestore } from 'angularfire2/firestore';
import { PhotoInfo } from './../../model/photoinfo.model';
import { SearchPlanPage } from './../search-plan/search-plan';
import { SearchPage } from './../search/search';
import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { UserManageProvider } from '../../providers/user-manage/user-manage';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  searchPlanPage: any;
  searchPage: any
  photoInfoCollection: AngularFirestoreCollection<PhotoInfo>;
  userInfoCollection: AngularFirestoreCollection<UserInfo>;

  constructor(public navCtrl: NavController, public firestore: AngularFirestore, public userManage: UserManageProvider, public toastCtrl: ToastController) {
    this.searchPlanPage = SearchPlanPage;
    this.searchPage = SearchPage;
    this.searchPlanPage = SearchPlanPage;
  }

  //おすすめの写真リストをデータベースから取得する
  getRecommendPhotoList(): Promise<PhotoInfo[]> {
    this.photoInfoCollection = this.firestore.collection('PhotoInfos');
    return new Promise(resolve => {
      this.photoInfoCollection.valueChanges().subscribe(data => {
        resolve(data);
      });
    });
  }

  getFavoritePhotoList(): Promise<PhotoInfo[]> {
    this.userInfoCollection = this.firestore.collection('UserInfos');
    this.photoInfoCollection = this.userInfoCollection.doc(this.userManage.getEmail()).collection('favoritesPhoto');
    return new Promise(resolve => {
      this.photoInfoCollection.valueChanges().subscribe(data => {
        resolve(data);
      });
    });
  }

  //おすすめのページに移動する
  async toRecommendPhotoPage() {
    let photoList = await this.getRecommendPhotoList();
    this.navCtrl.push(SearchPage, {
      photoList: photoList
    });
  }

  //お気に入り写真ページに移動する
  async toFavoritePhotoPage() {
    if (this.userManage.getStatus() == 'none') {
      this.showToast('ログインしてください');
      return;
    } else {
      let photoList = await this.getFavoritePhotoList();
      this.navCtrl.push(SearchPage, {
        photoList: photoList
      });
    }
  }

  //おすすめ旅行プランページに移動する
  toRecommendPlanPage() {
    var planInfoCollection = this.firestore.collection('PlanInfos');
    var planInfoList = [];
    var photoInfoList = [];
    planInfoCollection.valueChanges().subscribe(plandata => {
      var i = 0;
      plandata.forEach((plan: any) => {
        var photoInfoCollection = planInfoCollection.doc(plan.id).collection('list');
        photoInfoCollection.valueChanges().subscribe(photodata => {
          photoInfoList[i] = new Array();
          photodata.forEach(photo => {
            photoInfoList[i].push(photo);
          })
          i++;
        })
        planInfoList.push(plan);
      });
    });
    this.navCtrl.push(SearchPlanPage, {
      planInfoList: planInfoList,
      photoInfoList: photoInfoList
    })
  }

  //お気に入り旅行プランページに移動する
  toFavoritesPlanPage() {
    if (this.userManage.getStatus() == 'none') {
      this.showToast('ログインしてください');
      return;
    } else {
      var planInfoCollection = this.firestore.collection('UserInfos').doc(this.userManage.getEmail()).collection('favoritesPlan');
      var planInfoList = [];
      var photoInfoList = [];

      planInfoCollection.valueChanges().subscribe(plandata => {
        var i = 0;
        plandata.forEach((plan: any) => {
          var photoInfoCollection = planInfoCollection.doc(plan.id).collection('list');
          photoInfoCollection.valueChanges().subscribe(photodata => {
            photoInfoList[i] = new Array();
            photodata.forEach(photo => {
              photoInfoList[i].push(photo);
            })
            i++;
          })
          planInfoList.push(plan);
        });
      });
      this.navCtrl.push(SearchPlanPage, {
        planInfoList: planInfoList,
        photoInfoList: photoInfoList
      })
    }
  }

  showToast(message){
    this.toastCtrl.create({
      message: message,
      duration: 3000,
    }).present();
  }
}
