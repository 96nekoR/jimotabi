import { PhotoDetailPage } from './../photo-detail/photo-detail';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams} from 'ionic-angular';
import { PhotoInfo } from '../../model/photoinfo.model';

declare var Y: any;

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {

  photoList: PhotoInfo[];
  photoInfoList: PhotoInfo[][] = new Array();

  label:any;
  searchTerm: any = '';
  searchCategory: any = "すべて";
  isMapping: boolean = false;

  busStop:any = {
    place: [
      "三次駅前",
      "三次小学校前",
      "粟屋",
      "長谷",
      "船佐駅",
      "所木",
      "信木",
      "式敷駅",
      "香淀・香淀駅前",
      "作木口駅",
      "江平",
      "口羽",
      "羽須美支所",
      "伊賀和志",
      "宇都井駅",
      "石見都賀",
      "道の駅グリーンロード大和",
      "石見松原",
      "潮",
      "浜原駅前",
      "粕淵駅前",
      "簗瀬駅前",
      "乙原駅前",
      "竹駅",
      "木路原",
      "石見川本",
      "道の駅かわもと",
      "鹿賀",
      "川越",
      "大貫橋",
      "川戸",
      "川平",
      "千金",
      "江津本町",
      "江津駅前"
    ],
    latitude:[
      34.802609,
      34.811950,
      34.810132,
      34.803258,
      34.808004,
      34.810092,
      34.812855,
      34.815839,
      34.827196,
      34.860154,
      34.874872,
      34.884550,
      34.881840,
      34.894946,
      34.906456,
      34.956400,
      34.967584,
      34.992880,
      35.016191,
      35.059993,
      35.072527,
      35.042384,
      35.026295,
      35.012775,
      34.997569,
      34.991270,
      34.966581,
      34.963139,
      34.950006,
      34.951567,
      34.959554,
      34.996950,
      35.001640,
      35.007148,
      35.012886
    ],
    longitude:[
      132.857257,
      132.844387,
      132.812811,
      132.788102,
      132.766076,
      132.750895,
      132.730760,
      132.712818,
      132.713589,
      132.707223,
      132.702356,
      132.671262,
      132.668948,
      132.661369,
      132.629992,
      132.642801,
      132.641732,
      132.615119,
      132.632571,
      132.599638,
      132.593218,
      132.565873,
      132.547615,
      132.529940,
      132.508402,
      132.492600,
      132.473289,
      132.446067,
      132.415458,
      132.385982,
      132.333440,
      132.284675,
      132.249883,
      132.230272,
      132.223432
    ]
  }

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.filterPhotoInfoList();
  }

  ionViewDidLoad() {
    this.showMap();
  }

  showMap() {
    var map = new Y.Map("my-map", {
      configure: {
        mapType : Y.Map.TYPE.SMARTPHONE,
        doubleClickZoom: true,
        scrollWheelZoom: true,
        dragging: true
      }
    });
    map.drawMap(new Y.LatLng(34.7015, 135.4965), 8, Y.LayerSetId.NORMAL);

    var markers = [];
    for(var i=0; i<this.photoList.length; i++){
      var photo = this.photoList[i];
      //iconの設定
      var icon = new Y.Icon(photo.url, { iconSize: new Y.Size(48, 48) });
      //緯度経度の設定
      var lntlng = new Y.LatLng(photo.latitude, photo.longitude);
      markers[i] = new Y.Marker(lntlng, { title: photo.place, icon: icon });
      this.registerMarkerClickEvent(markers[i], photo);
    }
    markers = this.registerBusStopMarker(markers, map);

    map.addFeatures(markers);
  }

  registerBusStopMarker(markers, map){
    for(var i=0; i<35; i++){
      var lntlng = new Y.LatLng(this.busStop["latitude"][i], this.busStop["longitude"][i]);
      markers.push(new Y.Marker(lntlng, {title: this.busStop["place"][i]}));
      this.registerLabelClickEvent(markers[markers.length-1], lntlng, this.busStop["place"][i], map);
    }
    return markers;
  }

  //for内でbindすると値が固定されてしまうのでこのメソッドを挟んで詳細ページに移動する
  registerMarkerClickEvent(marker: any, photo: PhotoInfo) {
    marker.bind('click', () => {
      this.toPhotoDetailPage(photo);
    });
  }

  registerLabelClickEvent(marker: any, lntlng: any, name: any, map){
    marker.bind('click', ()=>{
      map.removeFeature(this.label);
      this.label = new Y.Label(lntlng, name);
      map.addFeature(this.label);
    })
  }

  //photoInfoListの配列の中身を初期化
  initPhotoList(){
    for(var i=0; i<this.photoInfoList.length; i++){
      this.photoInfoList[i].length = 0;
    }
    this.photoInfoList.length = 0;
  }

  //カテゴリーに一致するPhotoInfoのデータを取得する
  getPhotoList(){
    this.photoList = this.navParams.get('photoList');
    if (this.searchCategory != 'すべて') {
      this.photoList = this.photoList.filter(photo => photo.category == this.searchCategory);
    }
  }

  //取得したPhotoInfoのデータを二次元配列に変換する
  async filterPhotoInfoList() {
    this.getPhotoList();
    if (this.searchTerm && this.searchTerm.trim() !== '') {
      this.photoList = this.photoList.filter((photo: PhotoInfo) => {
        return photo.place.toLowerCase().includes(this.searchTerm.toLowerCase())
      });
    }
    this.initPhotoList();
    var row = 0;
    for (let i = 0; i < this.photoList.length; i+= 3) {
      this.photoInfoList[row] = new Array();
      this.photoInfoList[row] = this.photoList.slice(i, i + 3);
      row++;
    }
  }

  //詳細ページに移動する
  toPhotoDetailPage(photo: PhotoInfo) {
    this.navCtrl.push(PhotoDetailPage, {
      item: photo
    });
  }
}