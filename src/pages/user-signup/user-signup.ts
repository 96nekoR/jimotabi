import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';

import { UserInfo } from './../../model/userinfo.model';

import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { AngularFireStorage } from 'angularfire2/storage';

import { Camera, CameraOptions } from '@ionic-native/camera';
import { Observable } from 'rxjs/Observable';
import { finalize } from 'rxjs/operators';


@IonicPage()
@Component({
  selector: 'page-user-signup',
  templateUrl: 'user-signup.html',
})
export class UserSignupPage {

  private userinfosCollection: AngularFirestoreCollection<UserInfo>;
  userInfo: UserInfo;
  iconImageURL: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private storage: AngularFireStorage,
    private firestore: AngularFirestore,
    private afAuth: AngularFireAuth,
    private camera: Camera,
    private toast: ToastController
  ) {
    this.userinfosCollection = this.firestore.collection<UserInfo>('UserInfos');
    this.userInfo = {} as UserInfo;
  }

  // ユーザ登録
  signUpUser() {
    console.log(this.userInfo.name + " | " + this.userInfo.email + " | " + this.userInfo.password);

    this.afAuth
      .auth
      .createUserWithEmailAndPassword(this.userInfo.email, this.userInfo.password)
      .then(() => {
        this.uploadImg();
      })
      .catch(err => {
        this.presentToast(err.message);
      });
  }

  cropImage() {
    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM,
      saveToPhotoAlbum: false,
      allowEdit: true,
      targetWidth: 300,
      targetHeight: 300
    }
    this.camera.getPicture(options).then((ImageData) => {
      this.iconImageURL = 'data:image/jpeg;base64,' + ImageData;
    }, (err) => {
      console.log(err);
    });
  }

  //  Storageに写真ファイルを投稿
  uploadImg() {
    const filePath = "user-" + new Date().getTime() + '.jpg';
    const fileRef = this.storage.ref(filePath);
    const task = fileRef.putString(this.iconImageURL, 'data_url');
    task.snapshotChanges().pipe(
      finalize(() => {
        const downloadURL: Observable<string> = fileRef.getDownloadURL();
        this.setDownloadURL(downloadURL);
      }))
      .subscribe()
  }

  //  itemのurlにStorageのURLを代入
  setDownloadURL(_downloadURL: Observable<string>) {
    _downloadURL.subscribe((x: string) => {
      this.userInfo.iconImage = x;
      const id = this.userInfo.email;
      this.userInfo.travelId = null;
      this.userinfosCollection.doc(id).set(this.userInfo)
      .then(()=>{
        this.presentToast("登録できました。");
        this.navCtrl.pop();
      });
    })
  }

  presentToast(textMessage) {
    this.toast.create({
      message: textMessage,
      duration: 3000
    }).present();
  }

}
