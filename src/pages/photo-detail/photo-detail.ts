import { Storage } from '@ionic/storage';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { PhotoInfo } from '../../model/photoinfo.model';
import { UserManageProvider } from '../../providers/user-manage/user-manage';

@IonicPage()
@Component({
  selector: 'page-photo-detail',
  templateUrl: 'photo-detail.html',
})
export class PhotoDetailPage {

  item: PhotoInfo;
  isPhotoSaved: boolean = false;
  photoInfoCollection: AngularFirestoreCollection<PhotoInfo>;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public toastCtrl: ToastController,
    public firestore: AngularFirestore,
    public localstorage: Storage,
    public userManage: UserManageProvider
  ) {
    this.item = navParams.get('item');
    this.isFavoritePhoto();
  }

  //お気に入りに保存しているか保存していないか調べる
  isFavoritePhoto() {
    var userInfoCollection = this.firestore.collection('UserInfos');

    if (this.userManage.getStatus() == 'none') {
      return;
    } else {
      this.photoInfoCollection = userInfoCollection.doc(this.userManage.getEmail()).collection('favoritesPhoto');
      this.photoInfoCollection.valueChanges().subscribe(data => {
        this.isPhotoSaved = false;
        data.forEach(photo => {
          if (this.item.id == photo.id) {
            this.isPhotoSaved = true;
          }
        })
      })
    }
  }

  presentToast() {
    var status = this.userManage.getStatus();
    var message = "";
    //ログインしていないとき
    if (status == 'none') {
      message = "ログインしてください";
    } else {
      this.isPhotoSaved = !this.isPhotoSaved;
      var userInfoCollection = this.firestore.collection('UserInfos');
      if (this.isPhotoSaved) {
        message = "お気に入りに追加しました";
        userInfoCollection.doc(this.userManage.getEmail()).collection('favoritesPhoto').doc(this.item.id).set(this.item);
      } else {
        message = "お気に入りから削除しました";
        userInfoCollection.doc(this.userManage.getEmail()).collection('favoritesPhoto').doc(this.item.id).delete();
      }
    }
    const toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
    });
    toast.present();
  }

}
