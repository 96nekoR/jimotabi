import { UserEditPage } from './../pages/user-edit/user-edit';
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { HomePage } from '../pages/home/home';
import { SearchPage } from '../pages/search/search';
import { TabsPage } from '../pages/tabs/tabs';

import { PostPhotoinfoPage } from '../pages/post-photoinfo/post-photoinfo';
import { PhotoDetailPage } from '../pages/photo-detail/photo-detail';

import { UserInfoPage } from './../pages/user-info/user-info';
import { UserSignupPage } from './../pages/user-signup/user-signup';

import { SearchPlanPage } from '../pages/search-plan/search-plan';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Camera } from '@ionic-native/camera';
import { EmailComposer } from '@ionic-native/email-composer';

import { FIREBASE_CONFIG } from './firebase.credentials';
import { AngularFireModule } from 'angularfire2';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { UserManageProvider } from '../providers/user-manage/user-manage';
import { RoutePage } from '../pages/route/route';
import { PostPlanInfoPage } from '../pages/post-plan-info/post-plan-info';
import { IonicStorageModule } from '@ionic/storage';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TabsPage,
    PostPhotoinfoPage,
    PostPlanInfoPage,
    UserInfoPage,
    UserEditPage,
    UserSignupPage,
    SearchPage,
    SearchPlanPage,
    PhotoDetailPage,
    RoutePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    AngularFireModule.initializeApp(FIREBASE_CONFIG),
    AngularFireStorageModule,
    AngularFireDatabaseModule,
    AngularFirestoreModule,
    AngularFireAuthModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TabsPage,
    PostPhotoinfoPage,
    PostPlanInfoPage,
    UserInfoPage,
    UserEditPage,
    UserSignupPage,
    SearchPage,
    SearchPlanPage,
    PhotoDetailPage,
    RoutePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    EmailComposer,
    UserManageProvider,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
  ]
})
export class AppModule {}
