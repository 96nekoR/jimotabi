// ユーザ情報のモデル定義
export interface UserInfo {
    name: string;
    ID: string;
    email: string;
    password: string;
    comment: string;
    iconImage:string;
    travelId:string;
}
