// プラン情報のモデル定義
export interface PlanInfo {
    id:string;
    name: string;
    comment: string;
    userID: string;
    userEmail:string;
}