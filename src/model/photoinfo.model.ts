// 写真情報のモデル定義
export interface PhotoInfo {
    id: string;
    place: string;
    comment: string;
    url: string;
    category: string;
    latitude: number;
    longitude: number;
    userID: string;
    userEmail:string;
    userName: string;
    userIcon: string;
}
