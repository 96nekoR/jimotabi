import { UserInfo } from './../../model/userinfo.model';
import { Injectable } from '@angular/core';

@Injectable()
export class UserManageProvider {
  public email:string = "none";
  public status:string = "none";

  public userID:string;
  public uid:string;
  public myUserInfo: UserInfo;

  constructor() {
    this.myUserInfo = {} as UserInfo;
  }
  public getUserID()
  {
    if(this.userID == null){
      this.userID = "guest";
    }
    return this.userID;
  }

  public getUserInfo(){
    return this.myUserInfo;
  }

  public getStatus(){
    return this.status;
  }

  public setStatus(status){
    this.status = status;
  }

  public getEmail(){
    return this.email;
  }

  public setEmail(email){
    this.email = email;
  }

  public getUid(){
    return this.uid;
  }
}
